import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:mobile_gameteam/Lost.dart';
import 'package:mobile_gameteam/gamehome.dart';
import 'package:mobile_gameteam/symbol2.dart';
import 'package:mobile_gameteam/win.dart';


class Gamesymbol1 extends StatelessWidget {
  const Gamesymbol1({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
          color: Colors.black,
          child: ListView(
            children: [
              Container(
                child: Image.network("https://www.i-pic.info/i/ti1Z386947.jpg",width: 350,fit:BoxFit.cover),
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Column(
                    children: [
                      Container(
                          margin: EdgeInsets.all(15),
                          width: 200,
                          height: 40,
                          child: ElevatedButton(
                            child: Row(
                              mainAxisSize: MainAxisSize.min,
                              children: [
                                Text(
                                  "<",
                                  style: TextStyle(fontSize: 18),
                                ),
                              ],
                            ),
                            onPressed: () {
                              Navigator.push(
                                context,
                                MaterialPageRoute(
                                    builder: (context) => const LostPage()),
                              );
                            },
                          )),
                    ],
                  )
                ],
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Column(
                    children: [
                      Container(
                          margin: EdgeInsets.all(15),
                          width: 200,
                          height: 40,
                          child: ElevatedButton(
                            child: Row(
                              mainAxisSize: MainAxisSize.min,
                              children: [
                                Text(
                                  ">",
                                  style: TextStyle(fontSize: 18),
                                ),
                              ],
                            ),
                            onPressed: () {
                              Navigator.push(
                                context,
                                MaterialPageRoute(
                                    builder: (context) => const Gamesymbol2()),
                              );
                            },
                          )),
                    ],
                  )
                ],
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Column(
                    children: [
                      Container(
                          margin: EdgeInsets.all(15),
                          width: 200,
                          height: 40,
                          child: ElevatedButton(
                            child: Row(
                              mainAxisSize: MainAxisSize.min,
                              children: [
                                Text(
                                  "=",
                                  style: TextStyle(fontSize: 18),
                                ),
                              ],
                            ),
                            onPressed: () {
                              Navigator.push(
                                context,
                                MaterialPageRoute(
                                    builder: (context) => const LostPage()),
                              );
                            },
                          )),
                    ],
                  )
                ],
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Column(
                    children: [
                      Container(
                          margin: EdgeInsets.all(15),
                          child: ElevatedButton(
                            child: Row(
                              mainAxisSize: MainAxisSize.min,
                              children: [
                                Text(
                                  "HOME",
                                  style: TextStyle(fontSize: 18),
                                ),
                              ],
                            ),
                            onPressed: () {
                              Navigator.push(
                                context,
                                MaterialPageRoute(
                                    builder: (context) => const GameHome()),
                              );
                            },
                          )),
                    ],
                  )
                ],
              ),

            ],

          )

      ),
    );
  }
}
