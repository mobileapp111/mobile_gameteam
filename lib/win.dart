import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:mobile_gameteam/gamehome.dart';


class WinPage extends StatelessWidget {
  const WinPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
          color: Colors.black,
          child: ListView(
            children: [
              Container(
                child: Image.network("https://www.i-pic.info/i/rAg9386532.jpg",width: 750,height: 700),
              ),
              ElevatedButton(
                  onPressed: () {
                    Navigator.pushReplacement(context,
                        MaterialPageRoute(builder: (context) {
                          return GameHome();
                        }));
                  },
                  child: const Text('HOME')),
            ],

          )

      ),
    );
  }
}

