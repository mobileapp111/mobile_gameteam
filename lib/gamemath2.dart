import 'dart:math';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:mobile_gameteam/symbol1.dart';
import 'package:mobile_gameteam/win.dart';

import 'Lost.dart';
import 'gamehome.dart';


class Gamemath2 extends StatelessWidget {
  const Gamemath2({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
          color: Colors.black,
          child: ListView(
            children: [
              Container(
                child: Image.network(
                    "https://scontent.futp1-2.fna.fbcdn.net/v/t1.15752-9/334893787_1184994038875460_2619335935753846442_n.png?_nc_cat=108&ccb=1-7&_nc_sid=ae9488&_nc_eui2=AeH87p0pMqT9PVaYiUcJaAdssflmcxbTdrix-WZzFtN2uKLTuAb9ulxY3-U_ulwKBFwNjtivh_oBkeR1zHrGudQf&_nc_ohc=FM91idOJ7BIAX90GoCm&_nc_ht=scontent.futp1-2.fna&oh=03_AdTsuJGQ8m4msoWKiUBvIC5Y_ftPzSuzC_z77DvUbT4pcQ&oe=642D595C",
                    width: 100,
                    fit: BoxFit.cover),
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Column(
                    children: [
                      Container(
                        margin: EdgeInsets.all(15),
                        width: 200,
                        height: 40,
                        child: ElevatedButton(
                          child: Text(
                        generateRandomNumberAsString(),
                        style: TextStyle(fontSize: 18),
                      ),
                          onPressed: () {
                            Navigator.push(
                              context,
                              MaterialPageRoute(
                                builder: (context) => const LostPage(),
                              ),
                            );
                          },
                        ),
                      ),
                    ],
                  ),
                ],
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Column(
                    children: [
                      Container(
                          margin: EdgeInsets.all(15),
                          width: 200,
                          height: 40,
                          child: ElevatedButton(
                            child: Row(
                              mainAxisSize: MainAxisSize.min,
                              children: [
                                Text(
                                  "5",
                                  style: TextStyle(fontSize: 18),
                                ),
                              ],
                            ),
                            onPressed: () {
                              Navigator.push(
                                context,
                                MaterialPageRoute(
                                    builder: (context) => const Gamesymbol1()),
                              );
                            },
                          )),
                    ],
                  )
                ],
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Column(
                    children: [
                      Container(
                          margin: EdgeInsets.all(15),
                          width: 200,
                          height: 40,
                          child: ElevatedButton(
                            child: Row(
                              mainAxisSize: MainAxisSize.min,
                              children: [
                                Text(
                        generateRandomNumberAsString(),
                        style: TextStyle(fontSize: 18),
                      ),
                              ],
                            ),
                            onPressed: () {
                              Navigator.push(
                                context,
                                MaterialPageRoute(
                                    builder: (context) => const LostPage()),
                              );
                            },
                          )),
                    ],
                  )
                ],
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Column(
                    children: [
                      Container(
                          margin: EdgeInsets.all(15),
                          width: 200,
                          height: 40,
                          child: ElevatedButton(
                            child: Row(
                              mainAxisSize: MainAxisSize.min,
                              children: [
                                Text(
                        generateRandomNumberAsString(),
                        style: TextStyle(fontSize: 18),
                      ),
                              ],
                            ),
                            onPressed: () {
                              Navigator.push(
                                context,
                                MaterialPageRoute(
                                    builder: (context) => const LostPage()),
                              );
                            },
                          )),
                    ],
                  )
                ],
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Column(
                    children: [
                      Container(
                          margin: EdgeInsets.all(15),
                          child: ElevatedButton(
                            child: Row(
                              mainAxisSize: MainAxisSize.min,
                              children: [
                                Text(
                                  "HOME",
                                  style: TextStyle(fontSize: 18),
                                ),
                              ],
                            ),
                            onPressed: () {
                              Navigator.push(
                                context,
                                MaterialPageRoute(
                                    builder: (context) => const GameHome()),
                              );
                            },
                          )),
                    ],
                  )
                ],
              ),
            ],
          )),
    );
  }
}
String generateRandomNumberAsString() {
  Random random = new Random();
  int randomNumber;
  
  do {
    randomNumber = random.nextInt(11);
  } while (randomNumber == 5);
  
  return randomNumber.toString();
}


