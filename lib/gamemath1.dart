
import 'package:flutter/material.dart';
import 'package:mobile_gameteam/Lost.dart';
import 'package:mobile_gameteam/gamehome.dart';
import 'package:mobile_gameteam/gamemath2.dart';
import 'dart:math';
import 'package:mobile_gameteam/win.dart';

class Gamemath1 extends StatelessWidget {
  const Gamemath1({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
          color: Colors.black,
          child: ListView(
            children: [
              Container(
                child: Image.network(
                    "https://scontent.futp1-2.fna.fbcdn.net/v/t1.15752-9/334954174_600002921977497_1632480065762963114_n.png?_nc_cat=108&ccb=1-7&_nc_sid=ae9488&_nc_eui2=AeH-Ge9rLO_qvaLxMBO6pPQwt9oFjgRxquC32gWOBHGq4P9Npe_iU9YHSzC0Tksh_GSSz3d3U3zCXYcfSx30tOzt&_nc_ohc=kNjbWpwH4I4AX80vjeI&_nc_ht=scontent.futp1-2.fna&oh=03_AdRS79uCQ5yFTunU_2Wi1PmVdU38qV6dWAMZTVY5EjlgNA&oe=642D5B58",
                    width: 100,
                    fit: BoxFit.cover),
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Column(
                    children: [
                      Container(
                        margin: EdgeInsets.all(15),
                        width: 200,
                        height: 40,
                        child: ElevatedButton(
                          child: Text(
                        generateRandomNumberAsString(),
                        style: TextStyle(fontSize: 18),
                      ),
                          onPressed: () {
                            Navigator.push(
                              context,
                              MaterialPageRoute(
                                builder: (context) => const LostPage(),
                              ),
                            );
                          },
                        ),
                      ),
                    ],
                  ),
                ],
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Column(
                    children: [
                      Container(
                          margin: EdgeInsets.all(15),
                          width: 200,
                          height: 40,
                          child: ElevatedButton(
                            child: Row(
                              mainAxisSize: MainAxisSize.min,
                              children: [
                                Text(
                        generateRandomNumberAsString(),
                        style: TextStyle(fontSize: 18),
                      ),
                              ],
                            ),
                            onPressed: () {
                              Navigator.push(
                                context,
                                MaterialPageRoute(
                                    builder: (context) => const LostPage()),
                              );
                            },
                          )),
                    ],
                  )
                ],
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Column(
                    children: [
                      Container(
                          margin: EdgeInsets.all(15),
                          width: 200,
                          height: 40,
                          child: ElevatedButton(
                            child: Row(
                              mainAxisSize: MainAxisSize.min,
                              children: [
                                Text(
                        generateRandomNumberAsString(),
                        style: TextStyle(fontSize: 18),
                      ),
                              ],
                            ),
                            onPressed: () {
                              Navigator.push(
                                context,
                                MaterialPageRoute(
                                    builder: (context) => const LostPage()),
                              );
                            },
                          )),
                    ],
                  )
                ],
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Column(
                    children: [
                      Container(
                          margin: EdgeInsets.all(15),
                          width: 200,
                          height: 40,
                          child: ElevatedButton(
                            child: Row(
                              mainAxisSize: MainAxisSize.min,
                              children: [
                                Text(
                                  "4",
                                  style: TextStyle(fontSize: 18),
                                ),
                              ],
                            ),
                            onPressed: () {
                              Navigator.push(
                                context,
                                MaterialPageRoute(
                                    builder: (context) => const Gamemath2()),
                              );
                            },
                          )),
                    ],
                  )
                ],
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Column(
                    children: [
                      Container(
                          margin: EdgeInsets.all(15),
                          child: ElevatedButton(
                            child: Row(
                              mainAxisSize: MainAxisSize.min,
                              children: [
                                Text(
                                  "HOME",
                                  style: TextStyle(fontSize: 18),
                                ),
                              ],
                            ),
                            onPressed: () {
                              Navigator.push(
                                context,
                                MaterialPageRoute(
                                    builder: (context) => const GameHome()),
                              );
                            },
                          )),
                    ],
                  )
                ],
              ),
            ],
          )),
    );
  }
}
String generateRandomNumberAsString() {
  Random random = new Random();
  int randomNumber;
  
  do {
    randomNumber = random.nextInt(11);
  } while (randomNumber == 4);
  
  return randomNumber.toString();
}
