import 'package:flutter/material.dart';
import 'package:form_field_validator/form_field_validator.dart';
import 'package:mobile_gameteam/gamecountpic2.dart';

import 'Lost.dart';

class Gamecount1 extends StatelessWidget {
  final _formKey = GlobalKey<FormState>();
  final answerController = TextEditingController();
  String correctAnswer = '2';

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        color: Colors.black,
        child: ListView(
          children: [
            Container(
              child: Image.network(
                "https://www.i-pic.info/i/UpIT387086.jpg",
                height: 630,
                fit: BoxFit.cover,
              ),
            ),
            Form(key: _formKey, child: TextInput()),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Column(
                  children: [
                    Container(
                      margin: EdgeInsets.all(15),
                      width: 200,
                      height: 40,
                      child: ElevatedButton(
                        child: Row(
                          mainAxisSize: MainAxisSize.min,
                          children: [
                            Text(
                              "Submit",
                              style: TextStyle(fontSize: 18),
                            ),
                          ],
                        ),
                        onPressed: () {
                          if (_formKey.currentState!.validate()) {
                            String answer = answerController.text;
                            if (answer == correctAnswer) {
                              Navigator.push(
                                context,
                                MaterialPageRoute(
                                  builder: (context) => Gamecount2(),
                                ),
                              );
                            }
                          }
                          if (_formKey.currentState!.validate()) {
                            String answer = answerController.text;
                            if (answer != correctAnswer) {
                              Navigator.push(
                                context,
                                MaterialPageRoute(
                                  builder: (context) => LostPage(),
                                ),
                              );
                            }
                          }
                        },
                      ),
                    ),
                  ],
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }

  Widget TextInput() => TextFormField(
        controller: answerController,
        validator: MultiValidator(
            [RequiredValidator(errorText: "Please input answer")]),
        decoration: InputDecoration(
            hintText: 'Input your answer',
            hintStyle: TextStyle(color: Color.fromARGB(255, 178, 61, 233)),
            filled: true,
            fillColor: Color.fromARGB(186, 13, 231, 129),
            labelText: 'Answer',
            disabledBorder: UnderlineInputBorder(
                borderSide:
                    BorderSide(color: Color.fromARGB(255, 238, 234, 234)))),
        keyboardType: TextInputType.emailAddress,
        textInputAction: TextInputAction.done,
      );
}
