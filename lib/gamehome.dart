import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:mobile_gameteam/gamecountpic1.dart';


class GameHome extends StatelessWidget {
  const GameHome({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
          color: Colors.black,
          child: ListView(
            children: [
              Container(
                child: Image.network("https://www.i-pic.info/i/kxY1386548.jpg" ,width: 750,height: 700 ),
              ),
              ElevatedButton(
                  onPressed: () {
                    Navigator.pushReplacement(context,
                        MaterialPageRoute(builder: (context) {
                          return Gamecount1();
                        }));
                  },
                  child: const Text('START')),
            ],
          )
      ),
    );
  }
  }

