import 'package:device_preview/device_preview.dart';
import 'package:flutter/material.dart';
import 'package:mobile_gameteam/Lost.dart';
import 'package:mobile_gameteam/gamecountpic1.dart';
import 'package:mobile_gameteam/gamecountpic2.dart';
import 'package:mobile_gameteam/gamehome.dart';
import 'package:mobile_gameteam/gamemath1.dart';
import 'package:mobile_gameteam/gamemath2.dart';
import 'package:mobile_gameteam/symbol1.dart';
import 'package:mobile_gameteam/symbol2.dart';
import 'package:mobile_gameteam/win.dart';

import 'gamecountpic1.dart';

void main() {
  runApp(const Gameteam());
}

class Gameteam extends StatelessWidget {
  const Gameteam({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return DevicePreview(
      tools: const [
        DeviceSection(),
      ],
      builder: (context) => MaterialApp(
        debugShowCheckedModeBanner: false,
        useInheritedMediaQuery: true,
        builder: DevicePreview.appBuilder,
        locale: DevicePreview.locale(context),
        title: 'Smart Kids',
        theme: ThemeData(
          primarySwatch: Colors.purple,
        ),
        home: Scaffold(
          backgroundColor: Colors.black,
          body: const SafeArea(
            child: GameHome(),
          ),
        ),
      ),
    );
  }
}
